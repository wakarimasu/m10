<div><p align="center"><img src="m10cube-banner.jpg"></p></div>

<div><p align="center"><img src="m10cube.jpg" width="400"></p></div>

<p>Announcement 23/02/2022</p>
After a long time working endless hours on our WIZcube project and see it submitted to WIZnet contest.
Two Makers, totally strangers between them  one in Greece me and other in Denmark Olivier Gaillard teamed to go for the WIZnet contest and submit the idea before closing.
https://maker.wiznet.io/wiznet-ethernet-hat-contest/

We shared the same dream to build something different. Something to be useful to the community as being OSHW project.
It is a huge project and we must finish everything until the end of March.
During the building period it will be more info on our web www.wizcube.eu

<p align="center"><img src="cover.jpg"></p>

<p>Announcement 27/01/2022</p>
It is an attempt to present in few words, the work done so far and the new thoughts on M0CUBE’s future. A road map we may say.

It has been a year since we uploaded our first M10CUBE modules and explain in public the M10CUB concept here in GITLAB wiki page. 
All this time we are thinking and redesigning the M10CUBE hardware platform.
From the beginning we got an OSH license (GR000004) because we wanted our idea to be totally free for anybody to use.

Up to this time we do not have any radical ideas (but we are open to suggestions) to change the mechanical design, so we went into minor modifications for the shake of simplicity and compatibility. That can be found in the  Appendix.

Some limited standards must exists. Exactly like building a train. If you are a train builder the track width and maximum wagon high must followed. Then train design can be different and it is. Or an ISOBOX, or the bricks of LEGO.

As noticed from the start M0CUBE is closely related to Raspberry PI. It is our believe that Raspberry foundation and its team is the locomotive for creative development in the computer industry. Makers also will not go bankrupted following the “Raspberry Inside”.

M10CUBE’s mechanical design proved to be a hardware platform that any (almost) design can be possible. That is I.O.T and any Controller that interacts with the outside world comes in mind.
We have seen some very clever people having no connection each other to design on the almost the same mechanics as M10CUBE.  That is because of common sense was on board.

Beyond that M10CUBE concept as being modular, expandable, hardware “agnostic” and have enough area on board to join Arduino and Raspberry platforms (for instance putting an Arduino UNO or an ST NUCLEO64 module on board) permits us use the "M10CUBE Inside"  and the user will understand the mechanics involved.

That was not an easy task. Sometimes it was impossible to do what we want. So we had to improvise ways to overcome the problem. Not with success every time. The good thing is that we learn a lot in the process. That is both in mechanical and Electronic design. And we meat people whiling to helps us. The list of M0CUBE friends increases and that is not because of any money involved, but for the excitement on what can be designed and achieved (or not).

Having said that and considering some facts that gained from previews experience together with the new technological announcements we concluded that a radical approach must be taken on the design of any M0CUBE module.

Here are some of the new technical announcements that make us think a different hardware/firmware approach for M0CUBE  overall
- I3C
- I2C/1-Wire conversion chips
- New chips for supporting Electro-Chemical or Bio-Medical Sensing
- AI
- Very Fast RS485
- Ethernet 10BASE-T1L
- MATTER
- Shortage on I/O chips
- Flexibility more and different I/O and economics
- Edge computing
- Raspberry RP2040

As a designer you must take into account  all the above above and more. 
The meting point is computing on the edge. That can only achieved if an MPU exists on board.
A M10CUBE supporter triggered me an old idea
The idea of using an MPU on the I/O module is not new but now MPUs are 32 powerful monsters and prices are diving. That means with  a fraction of money you have AI on the Edge. Thus no bottlenecks on the network by sending to the cloud tons of data that can proceeded on the PCB level. On the Edge.
It s dead easy work for an MPU to do I/O extender or a PWM or A/D work.  It is cheaper to use an MPU instead of dedicated chips. I know it looks overkill but how cares? It is better for the environment too if you use the same device and program it to do different I/O jobs.
I do not thing of any reason we can not program it to behave as an MCPS2317 chip. I mean to put in place of it and the controlling MPU not knowing the difference?. 
I think the answer is Yes we can do it. Thus the module can behave as a slave module for a I/O extender, PWM, A/D, D/A  and the list is endles. All these having the ability to communicate with anything protocol possible I2C, I3C, SPI, RS485, CANBUS,  Ethernet and  more...

What changed now and this is feasible more that ever before? It is huge demand and production of RP2040 chip.
Raspberry foundation announced  that demand for the chip exploded and is producing millions. You can get one chip for less that a Euro.
The market will never have shortage of that chip.
A RP2040 design is already on the drawing board for our next generation of M0CUBE modules. Not only to substitute the special I/O chips but to create module with more function and intelligence on the Edge.

We will finish the modules we have already on production and testing becouse a lot of work is done on them but in the same time we will start an experimental line using RP2040.
Theese are:

<a href="https://hackaday.io/project/171770-m10cube/log/202613-m10cube-i2cspi-input-8x24v-output-8xhigh-side-double-io-chips">M10CUBE I2C/SPI INPUT 8X24V, OUTPUT ​8XHIGH SIDE (Double I/O chips)</a>

<a href="https://hackaday.io/project/171770-m10cube/log/202611-m10cube-i2cspi-input-8x24v-output-8xhigh-side">M10CUBE I2C/SPI INPUT 8X24V, OUTPUT ​8XHIGH SIDE</a>

<a href="https://hackaday.io/project/171770-m10cube/log/202098-m10cube-pico-cnc">M10CUBE PICO CNC</a>

<a href="https://hackaday.io/project/171770-m10cube/log/202097-m10cube-nucleo64-cnc">M10CUBE NUCLEO64 CNC</a>

<a href="https://hackaday.io/project/171770-m10cube/log/202093-m10cube-i2c-input-8x24v-output-8xssr-triac">M10CUBE I2C INPUT 8X24V, OUTPUT ​8XSSR TRIAC</a>

<a href="https://hackaday.io/project/171770-m10cube/log/202092-m10cube-i2c-8xrelay-module">M10CUBE I2C 8XRELAY MODUL</a>

A first board to start experimenting will be an I/O module using raspberry Pico module and see how it goes. Then if succeed in simulating, RP2040 will be our  MPU for every M10CUBE module.
In fact it triggered our interest an excellent python library we found and will do the  job out of the box by converting a Pico to a remote I/O extender, PWM, A/D, D/A and many more. At least it is a start.

Soon we publish our first M0CUBE I/O - RP2040 inside module and see how it goes.
If you find the idea interesting we would like to hear your comments. 
After all M10CUBE is a community project.
Enjoy

Appendix

M10CUBE standards
All the new versions have version V10 and above. Still this is under consideration to see if we are wrong with this standard. 
The modules are constructed and testing is underway for faults and correction.
Changed to 90X90mm (instead of the previews 91X91mm)PCB. 40 pin Raspberry Pi connector with the mounting holes. This is closer to Raspberry PI board.
Templates will be available to help makers. In KiCad V6.
When finished we will publish here the KiCad designs.

For the people to exchange M10CUBE hardware some limitations must exist and that is:
- 90X90mm PCB.
That is for the PCB to easily mount on a 100X100mm 3D printable box. 5mm space around will be for the internal PCB mounting gear. PCB can be extended on some cases to 100mm if the user needs an I/O connector located exactly on the face of the box. That is left to the designer to decide. Good practices are to not extend the side where the 40 pin RasPI. 
A PCB extension is good to have 10 or 15mm distance from the top/bottom of the 90X90 PCB. Templates will be published as well to help designers.
That is the case of our I/O modules and not only looks good but it is practical too (when one builds a box with opennings in place of I/O connectors everybody can use it).
2 - At least 4 mounting holes 3.1mm with 4mm distance from the corners on the 90X90mm PCB
3 - 40 pin connector 4mm at X Axis as is on Raspberry PI

----------


M10CUBE is a Practical Form Factor Ecosystem. It is the idea of a micro controller cube 10x10x10 cm.

SCOPE:
A control block to be used like a Lego "brick" to solve any control problems. Definitely a job for micro controllers. A smart design to be used as PLC or IOT device for Industry 4.0 and M2M deployments.

FIELD OF APPLICATIONS:
PLC, IOT, Education, Home Automation, Light Industrial Applications

PCBs, Schematics (Eagle and KiCAD), 3D enclosures and software are in constant developing.
Applications will keep coming so to encourage everyone to get involved and to use the M10CUBE out of the box

Soon...

A PLC software using all M10CUBE I/Os :

First PLC software will be  <a href="https://www.codesys.com/">CODESYS</a> (Raspberry Pi runtime is free for 2 hours use) since there is a lot of experience with this IDE.

Then other, totally free and open source IEC 61131-3 <a href="https://beremiz.org/">BEREMIZ</a> or IEC 61499 <a href="https://www.eclipse.org/4diac/">4DIAC</a> enabled IDEs can follow . Hopping someone will give us a little help on these brilliant software, following the well known song ...

A M10CUBE sensor board software:
That is written in Arduino IDE using all on board hardware running on the embedded ESP32 module.

More on <a href="https://gitlab.com/m10cube/m10/-/wikis/home">wiki</a> section

<H2>License</H2><p>
<img src="OSHWA-GR000004.jpg">

Verification code <a href="https://certification.oshwa.org/gr000004.html"> GR000004</a>

Licensed under the <a href="https://gitlab.com/m10cube/m10/-/blob/master/LICENCE.txt"> CERN OHL P 2.0 </a>
Software License: GPL v3<br>
Documentation License: CC BY 4.0 International<br> 

