EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 8 8
Title "M10SE01"
Date "2020-10-01"
Rev "1"
Comp "M10CUBE"
Comment1 ""
Comment2 "GPS, LoRa, RS485"
Comment3 "Temperature, Humidity, Barometric"
Comment4 "Ambient Light, UV Light, VOC, Sound"
$EndDescr
Connection ~ 1000 3575
Connection ~ 1000 3875
Wire Wire Line
	1450 3875 1450 4875
Wire Wire Line
	1000 3875 1450 3875
Wire Wire Line
	1450 4875 3000 4875
Wire Wire Line
	1000 3775 1000 3875
Wire Wire Line
	1000 3675 1000 3575
Wire Wire Line
	1000 3575 1450 3575
$Comp
L Connector_Generic:Conn_01x04 CON_?
U 1 1 5F70FED1
P 800 3675
AR Path="/5F70FED1" Ref="CON_?"  Part="1" 
AR Path="/5F6E481E/5F70FED1" Ref="CON_1"  Part="1" 
F 0 "CON_1" H 718 3992 50  0000 C CNN
F 1 "24V" H 718 3901 50  0000 C CNN
F 2 "Connector_Phoenix_MC:PhoenixContact_MC_1,5_4-G-3.81_1x04_P3.81mm_Horizontal" H 718 3342 50  0001 C CNN
F 3 "~" H 800 3675 50  0001 C CNN
	1    800  3675
	-1   0    0    -1  
$EndComp
$Comp
L Diode:B340 D?
U 1 1 5F70FED7
P 1600 3575
AR Path="/5F70FED7" Ref="D?"  Part="1" 
AR Path="/5F6E481E/5F70FED7" Ref="D1"  Part="1" 
F 0 "D1" H 1700 3594 59  0000 L BNN
F 1 "B340A" H 1700 3484 59  0000 L BNN
F 2 "DIODE:DO214AA" H 1600 3575 50  0001 C CNN
F 3 "" H 1600 3575 50  0001 C CNN
	1    1600 3575
	-1   0    0    1   
$EndComp
$Comp
L Device:C C?
U 1 1 5F70FEDD
P 3500 4175
AR Path="/5F70FEDD" Ref="C?"  Part="1" 
AR Path="/5F6E481E/5F70FEDD" Ref="C10"  Part="1" 
F 0 "C10" H 3660 4040 59  0000 L BNN
F 1 "100n" H 3660 4140 59  0000 L BNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 3500 4175 50  0001 C CNN
F 3 "" H 3500 4175 50  0001 C CNN
	1    3500 4175
	-1   0    0    1   
$EndComp
$Comp
L Device:Fuse F?
U 1 1 5F70FEE3
P 2500 3575
AR Path="/5F70FEE3" Ref="F?"  Part="1" 
AR Path="/5F6E481E/5F70FEE3" Ref="F1"  Part="1" 
F 0 "F1" H 2350 3630 59  0000 L BNN
F 1 "2A" H 2350 3460 59  0000 L BNN
F 2 "FUSE:TE5" H 2500 3575 50  0001 C CNN
F 3 "" H 2500 3575 50  0001 C CNN
	1    2500 3575
	0    1    1    0   
$EndComp
Text Label 3100 3575 0    70   ~ 0
24V_FUSE
Connection ~ 3500 3575
Connection ~ 3000 3575
Wire Wire Line
	3500 4025 3500 3575
Wire Wire Line
	3500 3575 4300 3575
Wire Wire Line
	3000 3575 3500 3575
Text Label 1750 3575 0    70   ~ 0
24V_DIODE
Wire Wire Line
	2350 3575 1750 3575
Connection ~ 3000 4875
Connection ~ 4450 4875
Connection ~ 3500 4875
Wire Wire Line
	4450 4875 5800 4875
Wire Wire Line
	3500 4325 3500 4875
Wire Wire Line
	3500 4875 3000 4875
Wire Wire Line
	4450 4875 3500 4875
Wire Wire Line
	4550 4525 4550 4425
Wire Wire Line
	4600 4525 4550 4525
Wire Wire Line
	4300 3575 4300 4425
Connection ~ 4550 4425
Wire Wire Line
	4600 4425 4550 4425
Wire Wire Line
	5700 4525 5700 4425
Wire Wire Line
	5600 4525 5700 4525
Wire Wire Line
	5600 4425 5700 4425
Wire Wire Line
	5600 4025 5800 4025
Wire Wire Line
	5800 3925 5600 3925
Wire Wire Line
	5800 4025 5800 3925
Connection ~ 5800 4025
Wire Wire Line
	5800 4875 5800 4025
Wire Wire Line
	4600 4025 4450 4025
Wire Wire Line
	4450 3925 4600 3925
Wire Wire Line
	4450 4025 4450 3925
Connection ~ 4450 4025
Wire Wire Line
	4450 4875 4450 4025
Wire Wire Line
	3000 4325 3000 4875
Wire Wire Line
	3000 4025 3000 3575
$Comp
L Device:CP C?
U 1 1 5F70FF0D
P 3000 4175
AR Path="/5F70FF0D" Ref="C?"  Part="1" 
AR Path="/5F6E481E/5F70FF0D" Ref="C9"  Part="1" 
F 0 "C9" H 2695 4144 59  0000 L BNN
F 1 "47µ / 35V" H 2445 4044 59  0000 L BNN
F 2 "Capacitor_SMD:CP_Elec_8x10" H 3000 4175 50  0001 C CNN
F 3 "" H 3000 4175 50  0001 C CNN
	1    3000 4175
	1    0    0    -1  
$EndComp
Wire Wire Line
	2650 3575 3000 3575
$Comp
L SWITCING:DC_DC-MP1584 U?
U 1 1 5F70FF1A
P 5000 4225
AR Path="/5F70FF1A" Ref="U?"  Part="1" 
AR Path="/5F6E481E/5F70FF1A" Ref="U4"  Part="1" 
F 0 "U4" H 5100 4811 59  0000 C CNN
F 1 "DC_DC-MP1584" H 5100 4706 59  0000 C CNN
F 2 "SWITCHING:DC_DC" H 5000 4225 50  0001 C CNN
F 3 "" H 5000 4225 50  0001 C CNN
	1    5000 4225
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F70FF20
P 1450 4875
AR Path="/5F70FF20" Ref="#PWR?"  Part="1" 
AR Path="/5F6E481E/5F70FF20" Ref="#PWR068"  Part="1" 
F 0 "#PWR068" H 1450 4625 50  0001 C CNN
F 1 "GND" H 1450 4725 50  0000 C CNN
F 2 "" H 1450 4875 50  0000 C CNN
F 3 "" H 1450 4875 50  0000 C CNN
	1    1450 4875
	1    0    0    -1  
$EndComp
Connection ~ 1450 4875
$Comp
L power:PWR_FLAG #FLG?
U 1 1 5F70FF27
P 4300 4425
AR Path="/5F70FF27" Ref="#FLG?"  Part="1" 
AR Path="/5F6E481E/5F70FF27" Ref="#FLG01"  Part="1" 
F 0 "#FLG01" H 4300 4500 50  0001 C CNN
F 1 "PWR_FLAG" H 4300 4598 50  0000 C CNN
F 2 "" H 4300 4425 50  0001 C CNN
F 3 "~" H 4300 4425 50  0001 C CNN
	1    4300 4425
	0    -1   -1   0   
$EndComp
Connection ~ 4300 4425
Connection ~ 5700 4425
Wire Wire Line
	4300 4425 4550 4425
Wire Wire Line
	5700 4425 5925 4425
Text Label 1000 3575 0    70   ~ 0
24V
$Comp
L power:GND #PWR?
U 1 1 5F70FF3B
P 9700 2675
AR Path="/5F70FF3B" Ref="#PWR?"  Part="1" 
AR Path="/5F6E481E/5F70FF3B" Ref="#PWR077"  Part="1" 
F 0 "#PWR077" H 9700 2425 50  0001 C CNN
F 1 "GND" H 9700 2525 50  0000 C CNN
F 2 "" H 9700 2675 50  0000 C CNN
F 3 "" H 9700 2675 50  0000 C CNN
	1    9700 2675
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F70FF41
P 9200 2675
AR Path="/5F70FF41" Ref="#PWR?"  Part="1" 
AR Path="/5F6E481E/5F70FF41" Ref="#PWR076"  Part="1" 
F 0 "#PWR076" H 9200 2425 50  0001 C CNN
F 1 "GND" H 9200 2525 50  0000 C CNN
F 2 "" H 9200 2675 50  0000 C CNN
F 3 "" H 9200 2675 50  0000 C CNN
	1    9200 2675
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F70FF47
P 8800 2675
AR Path="/5F70FF47" Ref="#PWR?"  Part="1" 
AR Path="/5F6E481E/5F70FF47" Ref="#PWR075"  Part="1" 
F 0 "#PWR075" H 8800 2425 50  0001 C CNN
F 1 "GND" H 8800 2525 50  0000 C CNN
F 2 "" H 8800 2675 50  0000 C CNN
F 3 "" H 8800 2675 50  0000 C CNN
	1    8800 2675
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F70FF4D
P 8500 2675
AR Path="/5F70FF4D" Ref="#PWR?"  Part="1" 
AR Path="/5F6E481E/5F70FF4D" Ref="#PWR074"  Part="1" 
F 0 "#PWR074" H 8500 2425 50  0001 C CNN
F 1 "GND" H 8500 2525 50  0000 C CNN
F 2 "" H 8500 2675 50  0000 C CNN
F 3 "" H 8500 2675 50  0000 C CNN
	1    8500 2675
	1    0    0    -1  
$EndComp
Text Notes 3275 5600 0    85   ~ 0
5V to 3.3V BUS
$Comp
L Device:C C?
U 1 1 5F70FF56
P 8500 2425
AR Path="/5F70FF56" Ref="C?"  Part="1" 
AR Path="/5EDCDBA8/5F70FF56" Ref="C?"  Part="1" 
AR Path="/5F6E481E/5F70FF56" Ref="C11"  Part="1" 
F 0 "C11" H 8560 2440 59  0000 L BNN
F 1 "10µF" H 8560 2240 59  0000 L BNN
F 2 "Capacitor_SMD:C_Elec_3x5.4" H 8500 2425 50  0001 C CNN
F 3 "" H 8500 2425 50  0001 C CNN
	1    8500 2425
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5F70FF5C
P 9700 2425
AR Path="/5F70FF5C" Ref="C?"  Part="1" 
AR Path="/5EDCDBA8/5F70FF5C" Ref="C?"  Part="1" 
AR Path="/5F6E481E/5F70FF5C" Ref="C13"  Part="1" 
F 0 "C13" H 9760 2440 59  0000 L BNN
F 1 "10µF" H 9760 2240 59  0000 L BNN
F 2 "Capacitor_SMD:CP_Elec_4x5.4" H 9700 2425 50  0001 C CNN
F 3 "" H 9700 2425 50  0001 C CNN
	1    9700 2425
	1    0    0    -1  
$EndComp
$Comp
L Regulator_Linear:LD1117S33TR_SOT223 IC?
U 1 1 5F70FF62
P 9200 2175
AR Path="/5F70FF62" Ref="IC?"  Part="1" 
AR Path="/5EDCDBA8/5F70FF62" Ref="IC?"  Part="1" 
AR Path="/5F6E481E/5F70FF62" Ref="IC3"  Part="1" 
F 0 "IC3" H 9300 1875 59  0000 L BNN
F 1 "TS1117" H 9300 1775 59  0000 L BNN
F 2 "Package_TO_SOT_SMD:SOT-223" H 9200 2175 50  0001 C CNN
F 3 "" H 9200 2175 50  0001 C CNN
	1    9200 2175
	1    0    0    -1  
$EndComp
$Comp
L Device:C C?
U 1 1 5F70FF68
P 8800 2425
AR Path="/5F70FF68" Ref="C?"  Part="1" 
AR Path="/5EDCDBA8/5F70FF68" Ref="C?"  Part="1" 
AR Path="/5F6E481E/5F70FF68" Ref="C12"  Part="1" 
F 0 "C12" H 8860 2440 59  0000 L BNN
F 1 "100n" H 8860 2240 59  0000 L BNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 8800 2425 50  0001 C CNN
F 3 "" H 8800 2425 50  0001 C CNN
	1    8800 2425
	1    0    0    -1  
$EndComp
Wire Wire Line
	9700 2175 9700 2275
Wire Wire Line
	9500 2175 9700 2175
Wire Wire Line
	8800 2275 8800 2175
Wire Wire Line
	8500 2275 8500 2175
Wire Wire Line
	8500 2175 7950 2175
Connection ~ 8500 2175
Wire Wire Line
	8800 2175 8500 2175
Connection ~ 8800 2175
Wire Wire Line
	8900 2175 8800 2175
Wire Wire Line
	8500 2675 8500 2575
Wire Wire Line
	9700 2675 9700 2575
Wire Wire Line
	9200 2675 9200 2475
Wire Wire Line
	8800 2675 8800 2575
Text Label 5925 4425 0    70   ~ 0
5V_MP1584
Text HLabel 1300 7300 0    50   Input ~ 0
3V3_BUS
$Comp
L power:+3.3V #PWR?
U 1 1 5F71D04C
P 1800 7100
AR Path="/5F0B8C7C/5F71D04C" Ref="#PWR?"  Part="1" 
AR Path="/5F6E481E/5F71D04C" Ref="#PWR069"  Part="1" 
F 0 "#PWR069" H 1800 6950 50  0001 C CNN
F 1 "+3.3V" H 1815 7273 50  0000 C CNN
F 2 "" H 1800 7100 50  0001 C CNN
F 3 "" H 1800 7100 50  0001 C CNN
	1    1800 7100
	0    1    1    0   
$EndComp
Wire Wire Line
	1800 7100 1800 7200
Connection ~ 1800 7100
Wire Wire Line
	1800 7200 1800 7300
Connection ~ 1800 7200
Text Notes 1200 6875 0    85   ~ 0
3.3V SELECTOR\n1-2 PSU\n3-4 ESP32\n5-6 BUS
Text Notes 2550 6775 0    85   ~ 0
5V SELECTOR\n1-2 BUS  \n2-3 MP1584
Text HLabel 2900 7050 0    50   Input ~ 0
5V_BUS
Text Label 9700 2175 0    50   ~ 0
3V3_PSU
Text HLabel 1300 7200 0    50   Input ~ 0
3V3_ESP32
Text Label 1300 7100 2    50   ~ 0
3V3_PSU
$Comp
L power:PWR_FLAG #FLG?
U 1 1 5F65CF45
P 5650 7175
AR Path="/5F65CF45" Ref="#FLG?"  Part="1" 
AR Path="/5F6E481E/5F65CF45" Ref="#FLG05"  Part="1" 
F 0 "#FLG05" H 5650 7250 50  0001 C CNN
F 1 "PWR_FLAG" H 5650 7348 50  0000 C CNN
F 2 "" H 5650 7175 50  0001 C CNN
F 3 "~" H 5650 7175 50  0001 C CNN
	1    5650 7175
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5F65CF4B
P 4325 7175
AR Path="/5F0A624D/5F65CF4B" Ref="#PWR?"  Part="1" 
AR Path="/5F65CF4B" Ref="#PWR?"  Part="1" 
AR Path="/5F6E481E/5F65CF4B" Ref="#PWR071"  Part="1" 
F 0 "#PWR071" H 4325 6925 50  0001 C CNN
F 1 "GND" H 4330 7002 50  0000 C CNN
F 2 "" H 4325 7175 50  0001 C CNN
F 3 "" H 4325 7175 50  0001 C CNN
	1    4325 7175
	1    0    0    -1  
$EndComp
$Comp
L power:PWR_FLAG #FLG?
U 1 1 5F65CF51
P 4325 7175
AR Path="/5F0A624D/5F65CF51" Ref="#FLG?"  Part="1" 
AR Path="/5F65CF51" Ref="#FLG?"  Part="1" 
AR Path="/5F6E481E/5F65CF51" Ref="#FLG02"  Part="1" 
F 0 "#FLG02" H 4325 7250 50  0001 C CNN
F 1 "PWR_FLAG" H 4325 7348 50  0000 C CNN
F 2 "" H 4325 7175 50  0001 C CNN
F 3 "~" H 4325 7175 50  0001 C CNN
	1    4325 7175
	1    0    0    -1  
$EndComp
Text Label 5650 7175 3    50   ~ 0
3V3_BUS
$Comp
L power:PWR_FLAG #FLG?
U 1 1 5F65CF58
P 4775 7175
AR Path="/5F65CF58" Ref="#FLG?"  Part="1" 
AR Path="/5F6E481E/5F65CF58" Ref="#FLG03"  Part="1" 
F 0 "#FLG03" H 4775 7250 50  0001 C CNN
F 1 "PWR_FLAG" H 4775 7348 50  0000 C CNN
F 2 "" H 4775 7175 50  0001 C CNN
F 3 "~" H 4775 7175 50  0001 C CNN
	1    4775 7175
	1    0    0    -1  
$EndComp
$Comp
L power:+3.3V #PWR?
U 1 1 5F65CF5E
P 4775 7175
AR Path="/5F65CF5E" Ref="#PWR?"  Part="1" 
AR Path="/5F6E481E/5F65CF5E" Ref="#PWR072"  Part="1" 
F 0 "#PWR072" H 4775 7025 50  0001 C CNN
F 1 "+3.3V" H 4790 7348 50  0000 C CNN
F 2 "" H 4775 7175 50  0001 C CNN
F 3 "" H 4775 7175 50  0001 C CNN
	1    4775 7175
	-1   0    0    1   
$EndComp
$Comp
L power:PWR_FLAG #FLG?
U 1 1 5F65CF64
P 6075 7175
AR Path="/5F65CF64" Ref="#FLG?"  Part="1" 
AR Path="/5F6E481E/5F65CF64" Ref="#FLG06"  Part="1" 
F 0 "#FLG06" H 6075 7250 50  0001 C CNN
F 1 "PWR_FLAG" H 6075 7348 50  0000 C CNN
F 2 "" H 6075 7175 50  0001 C CNN
F 3 "~" H 6075 7175 50  0001 C CNN
	1    6075 7175
	1    0    0    -1  
$EndComp
Text Label 6075 7175 3    50   ~ 0
5V_BUS
$Comp
L power:PWR_FLAG #FLG?
U 1 1 5F65CF6B
P 5225 7175
AR Path="/5F65CF6B" Ref="#FLG?"  Part="1" 
AR Path="/5F6E481E/5F65CF6B" Ref="#FLG04"  Part="1" 
F 0 "#FLG04" H 5225 7250 50  0001 C CNN
F 1 "PWR_FLAG" H 5225 7348 50  0000 C CNN
F 2 "" H 5225 7175 50  0001 C CNN
F 3 "~" H 5225 7175 50  0001 C CNN
	1    5225 7175
	1    0    0    -1  
$EndComp
$Comp
L power:+5V #PWR?
U 1 1 5F65CF71
P 5225 7175
AR Path="/5F65CF71" Ref="#PWR?"  Part="1" 
AR Path="/5F6E481E/5F65CF71" Ref="#PWR073"  Part="1" 
F 0 "#PWR073" H 5225 7025 50  0001 C CNN
F 1 "+5V" H 5240 7348 50  0000 C CNN
F 2 "" H 5225 7175 50  0001 C CNN
F 3 "" H 5225 7175 50  0001 C CNN
	1    5225 7175
	-1   0    0    1   
$EndComp
Wire Notes Line
	4125 7550 6425 7550
Wire Notes Line
	6425 7550 6425 6825
Wire Notes Line
	6425 6825 4125 6825
Wire Notes Line
	4125 6825 4125 7550
Text Notes 4400 6925 0    50   ~ 0
POWER FLAGS FOR SUPRESSING ERC ERRORS
$Comp
L power:+5V #PWR070
U 1 1 5F677B42
P 2900 7150
F 0 "#PWR070" H 2900 7000 50  0001 C CNN
F 1 "+5V" V 2915 7278 50  0000 L CNN
F 2 "" H 2900 7150 50  0001 C CNN
F 3 "" H 2900 7150 50  0001 C CNN
	1    2900 7150
	0    -1   -1   0   
$EndComp
Text Label 2900 7250 2    50   ~ 0
5V_MP1584
$Comp
L Connector_Generic:Conn_02x03_Odd_Even J?
U 1 1 5F71D044
P 1500 7200
AR Path="/5F71D044" Ref="J?"  Part="1" 
AR Path="/5F0B8C7C/5F71D044" Ref="J?"  Part="1" 
AR Path="/5F6E481E/5F71D044" Ref="J22"  Part="1" 
F 0 "J22" H 1500 7425 50  0000 C CNN
F 1 "3V3" V 1600 7200 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_2x03_P2.54mm_Vertical" H 1500 7200 50  0001 C CNN
F 3 "" H 1500 7200 50  0000 C CNN
	1    1500 7200
	1    0    0    -1  
$EndComp
$Comp
L Connector_Generic:Conn_01x03 J?
U 1 1 5F70FF14
P 3100 7150
AR Path="/5F70FF14" Ref="J?"  Part="1" 
AR Path="/5F6E481E/5F70FF14" Ref="J23"  Part="1" 
F 0 "J23" H 3100 7350 50  0000 C CNN
F 1 "5V" V 3200 7150 50  0000 C CNN
F 2 "Connector_PinHeader_2.54mm:PinHeader_1x03_P2.54mm_Vertical" H 3100 7150 50  0001 C CNN
F 3 "" H 3100 7150 50  0000 C CNN
	1    3100 7150
	1    0    0    -1  
$EndComp
Text Label 7950 2175 2    70   ~ 0
5V_MP1584
$Comp
L power:PWR_FLAG #FLG?
U 1 1 5F6BC8C3
P 8800 2175
AR Path="/5F6BC8C3" Ref="#FLG?"  Part="1" 
AR Path="/5F6E481E/5F6BC8C3" Ref="#FLG07"  Part="1" 
F 0 "#FLG07" H 8800 2250 50  0001 C CNN
F 1 "PWR_FLAG" H 8800 2348 50  0000 C CNN
F 2 "" H 8800 2175 50  0001 C CNN
F 3 "~" H 8800 2175 50  0001 C CNN
	1    8800 2175
	1    0    0    -1  
$EndComp
NoConn ~ 7325 3050
Wire Wire Line
	9500 2275 9500 2175
Connection ~ 9500 2175
$EndSCHEMATC
