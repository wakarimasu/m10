<div><p align="center"><img src="/MECHANICAL/IMAGES/m10-cpu-box.jpg"> <img src="/MECHANICAL/IMAGES/m10-cpu-box-feet.jpg"> <img src="/MECHANICAL/IMAGES/m10-io-box.jpg"></div>
<p></p>
<p><H1>PROPOSED DIMENTIONS for M10CUBE FRAMEWORK</H1></p>
<p>- External CUBE dimensions :100x100x100 mm</p>
<p>- Base (CPU) Frame width   :29.6 mm</p>
<p>- Add on frame width       :17.6</p>
<p>- PCB inside frames        :90x90 mm</p>
<p></p>
<p>Total M10CUBE will be      :1 CPU frame + 4 add on frames 29.6 + 4x17.6 = 100mm</p>
