28/09/2020

Naming conventions
Every M10CUBE module name contains 10 alphanumeric digits: M10XXXX-XX
1-3 Always M10 defining the project M10CUBE
4-5 The function of the module according to explanation below

DI Digital Input
DO Digital Output
DC Configurable as Input or Output DC
DX Digital Input/Output
AI Analog Input
AO Analog Output
AX Analog Input/Output
FM Pulse Train Outputs
PS Power Supply
CU Counter


6-7 Different type of board (but on the same function) but on the same category. 
eg the M10DO01-01 board is fixed now as an output board with 8 Relays of 16 amps. 
A future board of M10DO02-01 can be any number of relays or transistor or triac (or mixture of them) and capabilities BUS ALWAYS OUTPUT module

8 is the "-" separator
9-10 the version of the specific board
