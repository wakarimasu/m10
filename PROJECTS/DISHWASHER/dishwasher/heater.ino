bool updateTemperature() {
  if ((millis() - lastTempUpdate) > TEMP_READ_DELAY) {
    temperature = temperatureSensors.getTempFByIndex(0); //get temp reading
    lastTempUpdate = millis();
    temperatureSensors.requestTemperatures(); //request reading for next time
    return true;
  }
  return false;
}//void updateTemperature

void heater_control(){
 updateTemperature();
  setPoint = 200;
  myPID.run(); //call every loop, updates automatically at certain time interval
  
  analogWrite(state_db[heater_relay].condition, outputVal);
  digitalWrite(LED_BUILTIN, myPID.atSetPoint(1)); //light up LED when we're at setpoint +-1 degree
}

void heater_init(){
  temperatureSensors.begin();
  temperatureSensors.requestTemperatures();
  while (!updateTemperature()) {} //wait until temp sensor updated

  //if temperature is more than 4 degrees below or above setpoint, OUTPUT will be set to min or max respectively
  myPID.setBangBang(4);
  //set PID update interval to 4000ms
  myPID.setTimeStep(4000);
}
