
void refresh_modbus(){
    // Listen for incoming clients8  EthernetClient client = ethServer.available();
  EthernetClient client = ethServer.available();
  
  if(client) {
    // A new client connected
    Serial.println("new client");

    // let the Modbus TCP accept the connection 
    modbusTCPServer.accept(client);

    while(client.connected()) {
      // poll for Modbus TCP requests, while client connected
      modbusTCPServer.poll();

      // update the LED
      send_status();          //send back to CODESYS as LOOP BACK STATUS what is sent as outputs
      refresh_input_modbus(); //Fill Modbus input Registers with record [0]
      refresh_output_modbus();//Fill record [1]  with Modbus output registers
    }
    Serial.println("client disconnected");
  }
}

void refresh_input_modbus(void){
  uint16_t input = 0;
  for(int i = 0; i < 8; i++) {
    if(state_db[i].condition == HIGH)
      bitSet(input, i);
  }
  modbusTCPServer.holdingRegisterWrite(0x00, input);
}

void refresh_output_modbus(void){
  //Fill Output record with Modbus Output data
  for(int i = 0; i < 8; i++) {
    state_db[i+8].condition = modbusTCPServer.coilRead(i);
  }
}

void send_status(void){
  uint16_t stat = 0;
  for(int i = 0; i < 8; i++) {
    if(state_db[i+8].condition == HIGH)
      bitSet(stat, i);
  }
  modbusTCPServer.holdingRegisterWrite(0x01, stat);
}
