//Aplies to WIZcube modules M10DX01-20 and M10DX02-20 
//06/05/2022

//This is an try to convert into C of my dishwasher working with CODESYS 2
// using wizcube module M10DX02-20 https://www.wizcube.eu/dx2.html
//Thanks to this library https://github.com/stlehmann/arduino_plclib
//work in proggress

#include "header.h"

// define a module on data pin 8, clock pin 9 and strobe pin 7
byte leds;
byte keys;

/*' TFT DISPLAY **/
// Use hardware SPI
TFT_eSPI tft = TFT_eSPI();


void setup() {
  Serial.begin(115200); 

  // Init the Ethernet connection
  Ethernet.init(17);    // WIZnet W5100S-EVB-Pico SPI CS
  Ethernet.begin(mac, ip); //start the Ethernet connection and the server:
  Serial.print("MODBUS SERVER at ");
  Serial.println(Ethernet.localIP());
  
  // Start the Ethernet server
  ethServer.begin();
  
  // Start the Modbus TCP server
  if (!modbusTCPServer.begin()) {
    Serial.println("Failed to start Modbus TCP Server!");
    while (1);
  }

  // Configure the GPIOs
  config_gpio();   

  // Configure 8 coils at address 0x00
  modbusTCPServer.configureCoils(0x00, 8);
  // Configure 3 holding registers starting at address 0x00
  modbusTCPServer.configureHoldingRegisters(0x00, 3);
  
  reset_outputs();
  
  #if BUZZER
    BUZZER_init();
     cricket(2); // Initialisation OK sound
  #endif  
       
  //pinMode(LED_BUILTIN, OUTPUT);
}

void loop(){
  dishwasher_cycle();
  refresh_io();
  //refresh_modbus();
  //in_out_test(); 
  //test_IEC61131_3();
  //flash_outputs();
}
