<b>WIZcube Beremiz</b><p>

Beremiz (beremiz.org) by Edouard Tisseran is the open source PLC programming . That is IEC-61131 among other standards.
https://github.com/beremiz/beremiz
Beremiz has many years of development and is the ONLY one open source 
IEC-61131programming tools that is ported and working for many years in industrial controllers as seen by the use cases.
https://beremiz.org/usecases
It relies on open standards to be independent of the targeted device, and let you turn any processor into a PLC. Beremiz includes tools to create HMI, and to connect your PLC programs to existing supervisions, databases, or fieldbuses.<p>

With Beremiz, you conform to standards, avoid vendor lock, and contribute to the better future of Automation.<p>

See official Beremiz website for more information.<p>

Detailed information can be found in the <a href="https://github.com/beremiz/beremiz" target="_blank"><b>Beremiz GitLab</b></a>

Soon many programs will be releasd using WIZcube modules. 
That is to comply with my old dreem back in 2011 as I stated it in the Raspberyy forum <a href="https://www.raspberrypi.org/forums/viewtopic.php?f=41&t=455&hilit=vorrias
" target="_blank"><b>Turn Raspberry Pi into a P.L.C</b></a>

Update 19/06/2022<br>
For this project to work, we only need the Raspberry Pico as IEC-6113 run time controller and a WIZcube module for I/O.<br>
Thanks to Mr Tisserant and Mr Bouchez this project is a reality<br>
We are exited to present the first BEREMIZ + Wizcube project.<br>
Some time ago Beremiz was ported in Raspberry Pico. This excellent porting was done by Mr Benoit Bouchez.<br>
https://github.com/bbouchez/Beremiz4Pico<p>

Followed exactly the detailed instructions given by Mr Bouchez we loaded to our wizcube TRIAC module M10DX02-20.<br>

<div><p align="center"><img src="beremiz-m10dx02.jpg"></div>

That is one of our industrial grade I/O with Raspberry Pico.<br>
https://gitlab.com/m10cube/m10/-/tree/master/ELECTRICAL/INPUT%20OUTPUT/M10DX02-20<br>
The project is extremely simple and has only to saw that IEC-6113 programming is possible on Raspberry Pico.<br>
What the program does and can be seen from the video is to direct inputs to outputs. See it in the <a href="M10DX01-20.pdf"> pdf</a> source program document provided. <br>
Inputs 1,2,3,4 drive outputs 1,2,3,4 via 1,2,3 and 4 sec delay. Inputs 5,6,7,8 drive outputs 5,6,7,8  directly without delay. Outputs are negated to confirm with hardware demands. TRIAC outputs can drive 220v up to 16A load.<br>
That is only a demo for proof of concept. We are preparing a full scale DISHWASHER application. As said before that is written (and working) in CODESYS2 and now we are transferring it to BEREMIZ and then upload it in one of our wizcube Pico modules.<br>
The difficult part was to make this simple program. The easy part is to make a very complicated program.<br>
The video https://youtu.be/uUzTIrfelbs

<p>Stay tuned for the BEREMIZ + RASPBERRY PICO washing machine.

<H2>License</H2><p>
<img src="OSHWA-GR000004.jpg">

Verification code <a href="https://certification.oshwa.org/gr000004.html"> GR000004</a>

Licensed under the <a href="https://gitlab.com/m10cube/m10/-/blob/master/LICENCE.txt"> CERN OHL P 2.0 </a>
Software License: GPL v3<br>
Documentation License: CC BY 4.0 International<br> 





