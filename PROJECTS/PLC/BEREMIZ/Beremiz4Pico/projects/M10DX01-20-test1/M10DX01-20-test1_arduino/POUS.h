#include "beremiz.h"
#ifndef __POUS_H
#define __POUS_H

#include "accessor.h"
#include "iec_std_lib.h"

// PROGRAM PROGRAM0
// Data part
typedef struct {
  // PROGRAM Interface - IN, OUT, IN_OUT variables

  // PROGRAM private variables - TEMP, private and located variables
  __DECLARE_LOCATED(BOOL,LED1)
  __DECLARE_LOCATED(BOOL,LED2)
  __DECLARE_LOCATED(BOOL,LED3)
  __DECLARE_LOCATED(BOOL,LED4)
  __DECLARE_LOCATED(BOOL,LED5)
  __DECLARE_LOCATED(BOOL,LED6)
  __DECLARE_LOCATED(BOOL,LED7)
  __DECLARE_LOCATED(BOOL,LED8)
  __DECLARE_LOCATED(BOOL,SW1)
  __DECLARE_LOCATED(BOOL,SW2)
  __DECLARE_LOCATED(BOOL,SW3)
  __DECLARE_LOCATED(BOOL,SW4)
  __DECLARE_LOCATED(BOOL,SW5)
  __DECLARE_LOCATED(BOOL,SW6)
  __DECLARE_LOCATED(BOOL,SW7)
  __DECLARE_LOCATED(BOOL,SW8)
  TOF TOF1;
  TOF TOF2;
  TOF TOF3;
  TOF TOF4;

} PROGRAM0;

void PROGRAM0_init__(PROGRAM0 *data__, BOOL retain);
// Code part
void PROGRAM0_body__(PROGRAM0 *data__);
#endif //__POUS_H
