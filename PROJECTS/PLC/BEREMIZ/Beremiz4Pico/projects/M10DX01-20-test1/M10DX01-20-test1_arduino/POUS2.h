void PROGRAM0_init__(PROGRAM0 *data__, BOOL retain) {
  __INIT_LOCATED(BOOL,__QX0_4,data__->LED1,retain)
  __INIT_LOCATED_VALUE(data__->LED1,__BOOL_LITERAL(FALSE))
  __INIT_LOCATED(BOOL,__QX0_6,data__->LED2,retain)
  __INIT_LOCATED_VALUE(data__->LED2,__BOOL_LITERAL(FALSE))
  __INIT_LOCATED(BOOL,__QX0_8,data__->LED3,retain)
  __INIT_LOCATED_VALUE(data__->LED3,__BOOL_LITERAL(FALSE))
  __INIT_LOCATED(BOOL,__QX0_10,data__->LED4,retain)
  __INIT_LOCATED_VALUE(data__->LED4,__BOOL_LITERAL(FALSE))
  __INIT_LOCATED(BOOL,__QX0_12,data__->LED5,retain)
  __INIT_LOCATED_VALUE(data__->LED5,__BOOL_LITERAL(FALSE))
  __INIT_LOCATED(BOOL,__QX0_14,data__->LED6,retain)
  __INIT_LOCATED_VALUE(data__->LED6,__BOOL_LITERAL(FALSE))
  __INIT_LOCATED(BOOL,__QX0_26,data__->LED7,retain)
  __INIT_LOCATED_VALUE(data__->LED7,__BOOL_LITERAL(FALSE))
  __INIT_LOCATED(BOOL,__QX0_28,data__->LED8,retain)
  __INIT_LOCATED_VALUE(data__->LED8,__BOOL_LITERAL(FALSE))
  __INIT_LOCATED(BOOL,__IX0_5,data__->SW1,retain)
  __INIT_LOCATED_VALUE(data__->SW1,__BOOL_LITERAL(FALSE))
  __INIT_LOCATED(BOOL,__IX0_7,data__->SW2,retain)
  __INIT_LOCATED_VALUE(data__->SW2,__BOOL_LITERAL(FALSE))
  __INIT_LOCATED(BOOL,__IX0_9,data__->SW3,retain)
  __INIT_LOCATED_VALUE(data__->SW3,__BOOL_LITERAL(FALSE))
  __INIT_LOCATED(BOOL,__IX0_11,data__->SW4,retain)
  __INIT_LOCATED_VALUE(data__->SW4,__BOOL_LITERAL(FALSE))
  __INIT_LOCATED(BOOL,__IX0_13,data__->SW5,retain)
  __INIT_LOCATED_VALUE(data__->SW5,__BOOL_LITERAL(FALSE))
  __INIT_LOCATED(BOOL,__IX0_15,data__->SW6,retain)
  __INIT_LOCATED_VALUE(data__->SW6,__BOOL_LITERAL(FALSE))
  __INIT_LOCATED(BOOL,__IX0_22,data__->SW7,retain)
  __INIT_LOCATED_VALUE(data__->SW7,__BOOL_LITERAL(FALSE))
  __INIT_LOCATED(BOOL,__IX0_27,data__->SW8,retain)
  __INIT_LOCATED_VALUE(data__->SW8,__BOOL_LITERAL(FALSE))
  TOF_init__(&data__->TOF1,retain);
  TOF_init__(&data__->TOF2,retain);
  TOF_init__(&data__->TOF3,retain);
  TOF_init__(&data__->TOF4,retain);
}

// Code part
void PROGRAM0_body__(PROGRAM0 *data__) {
  // Initialise TEMP variables

  __SET_VAR(data__->TOF1.,IN,,__GET_LOCATED(data__->SW1,));
  __SET_VAR(data__->TOF1.,PT,,__time_to_timespec(1, 0, 1, 0, 0, 0));
  TOF_body__(&data__->TOF1);
  __SET_LOCATED(data__->,LED1,,!(__GET_VAR(data__->TOF1.Q,)));
  __SET_VAR(data__->TOF2.,IN,,__GET_LOCATED(data__->SW2,));
  __SET_VAR(data__->TOF2.,PT,,__time_to_timespec(1, 0, 2, 0, 0, 0));
  TOF_body__(&data__->TOF2);
  __SET_LOCATED(data__->,LED2,,!(__GET_VAR(data__->TOF2.Q,)));
  __SET_VAR(data__->TOF3.,IN,,__GET_LOCATED(data__->SW3,));
  __SET_VAR(data__->TOF3.,PT,,__time_to_timespec(1, 0, 3, 0, 0, 0));
  TOF_body__(&data__->TOF3);
  __SET_LOCATED(data__->,LED3,,!(__GET_VAR(data__->TOF3.Q,)));
  __SET_VAR(data__->TOF4.,IN,,__GET_LOCATED(data__->SW4,));
  __SET_VAR(data__->TOF4.,PT,,__time_to_timespec(1, 0, 4, 0, 0, 0));
  TOF_body__(&data__->TOF4);
  __SET_LOCATED(data__->,LED4,,!(__GET_VAR(data__->TOF4.Q,)));
  __SET_LOCATED(data__->,LED5,,!(__GET_LOCATED(data__->SW5,)));
  __SET_LOCATED(data__->,LED6,,!(__GET_LOCATED(data__->SW6,)));
  __SET_LOCATED(data__->,LED7,,!(__GET_LOCATED(data__->SW7,)));
  __SET_LOCATED(data__->,LED8,,!(__GET_LOCATED(data__->SW8,)));

  goto __end;

__end:
  return;
} // PROGRAM0_body__() 





