void send_status(void){
  uint16_t stat = 0;
  for(int i = 0; i < 8; i++) {
    if(state_db[i+8].condition == HIGH)
      bitSet(stat, i);
  }
  modbusTCPServer.holdingRegisterWrite(0x01, stat);
}

void config_gpio(void){
  for(int i = 0; i < 8; i++) {
    pinMode(gpio[i], INPUT);
    pinMode(gpio[i + 8], OUTPUT);
  }       
}

void refresh_input(void){
  for(int i = 0; i < 8; i++) {
    state_db[i].condition = !digitalRead(gpio[i]);
  } 
}

void refresh_output(void){
  for(int i = 0; i < 8; i++) {
    digitalWrite(gpio[i + 8], state_db[i + 8].condition);
  }
}

void refresh_input_modbus(void){
  uint16_t input = 0;
  for(int i = 0; i < 8; i++) {
    if(state_db[i].condition == HIGH)
      bitSet(input, i);
  }
  modbusTCPServer.holdingRegisterWrite(0x00, input);
}

void refresh_output_modbus(void){
  //Fill Output record with Modbus Output data
  for(int i = 0; i < 8; i++) {
    state_db[i+8].condition = modbusTCPServer.coilRead(i);
  }
}
