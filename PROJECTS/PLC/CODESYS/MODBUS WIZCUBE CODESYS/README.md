<b>PROJECT:  MODBUS WIZCUBE CODESYS</b><br>
This is a test project for people using CODESYS environment to see the WIZcube module M10DX01-20 working and produce results
WIZcube modules M10RL01-20 Relay and M1DXL02-20 TRIAC can also be use with the same CODESYS driver<p>
<div><p align="center"><img src="visualisation.jpg"></p></div>
A short video with Raspberry Pi Zero WiFi under the module executing a <a href=https://www.youtube.com/watch?v=dg2AYa0JUbg&t=2s&ab_channel=M10CUBE" target="_blank"><b>PLC MODBUS COMMUNICATION </a>test program "running lights can be seen.<br>
Modbus command travelling from Raspberry WiFi to WIZcube M10RL01-20 Relay module and executed. The visualisation can be seen on the laptop. WIZcube stand alone module can be anywhere in the network executing the command from the Raspberry PI. In fact we are preparing a setup with the WIZcube M10RL01-20 Relay module, located in Denmark where the other team member Olivier lives.<p>

<p>More tests or real working examples, will uploaded as soon as we finish them.</p>

<H2>License</H2><p>
<img src="OSHWA-GR000004.jpg">

Verification code <a href="https://certification.oshwa.org/gr000004.html"> GR000004</a>

Licensed under the <a href="https://gitlab.com/m10cube/m10/-/blob/master/LICENCE.txt"> CERN OHL P 2.0 </a>
Software License: GPL v3<br>
Documentation License: CC BY 4.0 International<br> 





