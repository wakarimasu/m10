#include "MQTT_MESSAGE.h"

void MQTT_init() {
  /*
    if (!bme.begin(0x76)){  
        USE_SERIAL.println("Could not find a valid BME280 sensor, check wiring!");
        while (1);
    }
    */    
    ETHERNET_init();  
    client.setServer(mqtt_server, 1883);
}

void db_init(){
    //Fill databese sensor with MQTT topics
    soil_db[0].topic_moisture = "moisture_1";
    soil_db[1].topic_moisture = "moisture_2"; 
    soil_db[2].topic_moisture = "moisture_3";
    soil_db[3].topic_moisture = "moisture_4";      
    soil_db[4].topic_moisture = "moisture_5";
    soil_db[5].topic_moisture = "moisture_6"; 
    soil_db[6].topic_moisture = "moisture_7";
    soil_db[7].topic_moisture = "moisture_8";
    soil_db[0].topic_soil = "soil_1";
    soil_db[1].topic_soil = "soil_2";
    soil_db[2].topic_soil = "soil_3";
    soil_db[3].topic_soil = "soil_4";
    soil_db[4].topic_soil = "soil_5";
    soil_db[5].topic_soil = "soil_6";      
    soil_db[6].topic_soil = "soil_7";
    soil_db[7].topic_soil = "soil_8"; 
}

void refresh_mqtt(){
   //READ SENSOR AND SEND MQTT
  String mqttstr;
  char msg[10];
  char msgtext[25];
  char msgtext_[25];
  char moisture_[10];
  char soil_[10];
  int n; 
  while (n < channel){
    if (!client.connected()) {
      reconnect();
    }
    if(soil_db[n].adc < 50) {
    mqttstr = message_1 + String(n+1) + message_2 + message_3 + String(n+1) + message_4;
    mqttstr.toCharArray(msgtext_, 50);
    sprintf(msgtext,msgtext_,soil_db[n].adc);
  }   
  else if (soil_db[n].adc >= 50){
    mqttstr = message_1 + String(n+1) + message_6 + message_3 + String(n+1) + message_5;
    mqttstr.toCharArray(msgtext_, 50);   
    sprintf(msgtext,msgtext_,soil_db[n].adc); 
  }
  else  
  {
      sprintf(msgtext,"Sensor Problem",soil_db[n].adc); 
  }
  sprintf(msg,"%i",soil_db[n].adc);
  strcpy( joint_percent, msg );
  strcat( joint_percent, " %" );   
  (soil_db[n].topic_moisture).toCharArray(moisture_, 25);
  (soil_db[n].topic_soil).toCharArray(soil_, 25);
  client.publish(moisture_, joint_percent);
  client.publish(soil_, msgtext);
   //Serial.println(moisture_);
   //Serial.println(joint_percent);
    n++;
  } 
  //delay(5000);
}

void reconnect() {
  // Loop until we're reconnected
  Serial.println("In reconnect...");
  if (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Attempt to connect
    if (client.connect("WIZcube_SOIL_Moisture")) {
      Serial.println("connected");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 2 seconds");
      delay(2000);
    }
  }
}
