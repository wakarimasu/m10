///Modifided by www.wizcube.eu 08/05/2022
// FILE: ADS_continuous_4_channel.ino
// AUTHOR: Rob.Tillaart
// VERSION: 0.1.2
// PURPOSE: read multiple analog inputs continuously
//          interrupt driven to catch all conversions.

// test
// connect multiple potmeters
//
// GND ---[   x   ]------ 5V
//            |
//
// measure at x  - connect to AIN0..4.
//
// for the test it is good to have AIN3 connected to 5V and AIN4 to GND
// so one can see these as references in the output.
//

#include "ADC_ADS1115.h"
void ADC_ADS1115_init(){

  Serial.println(__FILE__);
  Serial.print("ADS1X15_LIB_VERSION: ");
  Serial.println(ADS1X15_LIB_VERSION);

  ADS.begin();
  ADS.setGain(0);        // 1mv = 1 bit
  ADS.setDataRate(7);    // slow

  // SET ALERT RDY PIN
  ADS.setComparatorThresholdHigh(0x8000);
  ADS.setComparatorThresholdLow(0x0000);
  ADS.setComparatorQueConvert(0);

  // SET INTERRUPT HANDLER TO CATCH CONVERSION READY
  //pinMode(2, INPUT_PULLUP);
  //attachInterrupt(digitalPinToInterrupt(2), adsReady, RISING);

  ADS.setMode(0);     // continuous mode
  ADS.readADC(0);     // trigger first read
}

void ADC_ADS1115_read(){
    int n;
    while (n < channel){
      ADS.readADC(n);
      int soil_moisture = map(ADS.getValue(), 6000, 15000, 100, 0); //ADC value wet, dry, 100, 0
         if (soil_moisture >= 100)
          soil_moisture = 100;
         if (soil_moisture <= 0 )
          soil_moisture = 0; 
         soil_db[n].adc = soil_moisture;
         Serial.println(ADS.getValue());
      n++;
    }
    Serial.println();
 }
