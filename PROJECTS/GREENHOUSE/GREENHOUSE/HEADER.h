#include <EthernetClient.h>
#include <Ethernet.h>
#include <SPI.h>
#include "BUZZER.h"
#include "ADC_ADS1115.h"
#define BUZZER_SEM 1
#define ADC_ADS1115 1
#define MODBUS 0

#if MODBUS
  #include <ArduinoModbus.h>
  #include <ArduinoRS485.h>
#endif

//MESSAGES
String message_1 = "SOIL ";
String message_2 = " NEEDS WATTER ";
String message_3 = " VALVE ";
String message_4 = " ON";
String message_5 = " OFF";
String message_6 = " IS OK";

String mqttstr;
char msg[10];
char msgtext[25];
char msgtext_[25];
char moisture_[10];
char soil_[10];
char joint_percent[20]; 

//Define OUTPUT
//pins on Wiznet Pico clone
int OUT_1 = 4;
int OUT_2 = 5;
int OUT_3 = 6;
int OUT_4 = 7;
int OUT_5 = 8;
int OUT_6 = 9;
int OUT_7 = 10;
int OUT_8 = 11;

byte mac[] = {  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED};  // Define MAC address
int gpio[] = {0,0,0,0,0,0,0,0,OUT_1,OUT_2,OUT_3,OUT_4,OUT_5,OUT_6,OUT_7,OUT_8};
IPAddress ip(192, 168, 1, 200);
IPAddress myDns(192, 168, 1, 1);
IPAddress gateway(192, 168, 1, 1);
IPAddress subnet(255, 255, 255, 0);
//EthernetServer ethServer(502);



//INPUT/OUTPUT STATES DATABASE
typedef struct  {
  bool condition; 
} database_t;
  database_t  state_db[16];
  
  //Define INPUT pins on Wiznet Pico clone
#define VALVE_1 8                //Wattering valve 1
#define VALVE_2 9                //Wattering valve 2
#define VALVE_3 10               //Wattering valve 3
#define VALVE_4 11               //Wattering valve 4 
#define VALVE_5 12               //Wattering valve 5
#define VALVE_6 13               //Wattering valve 6
#define VALVE_7 14               //Wattering valve 7
#define VALVE_8 15               //Wattering valve 8 


//SENSOR DATABASE
typedef struct  {
  int id; 
  int16_t adc;
  bool valve;
  String topic_soil;      //contain the message concerning the action
  String topic_moisture;  //contail the moistute value
} database_l;
  database_l  soil_db[16];

  uint8_t channel = 4; //# of ADC channels

  unsigned long control_task_timer = 0;           //CHANGE REFRESH READING TIME THAT IF YOU LIKE
  unsigned long mqtt_task_timer = 0;              //CHANGE BLINK "I AM ILIVE" TIMER IF YOU LIKE .
  unsigned long control_loop_time = 1000;
  unsigned long mqtt_loop_time = 10000; 
