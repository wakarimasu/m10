#ifndef MQTT_MESSAGE_H
#define MQTT_MESSAGE_H
  //#include <Arduino.h>
  //#include <stdio.h>
  //#include "mbed/dtostrf.h"
  //#include <Wire.h>
  //#include "Adafruit_Sensor.h"
  //#include <Adafruit_BME280.h>
    #include <PubSubClient.h>
  
  EthernetClient wizcube;
  PubSubClient client(wizcube);
  const char* mqtt_server = "192.168.1.50";
    
  void MQTT_init();
  void ETHERNET_init();
  void MQTT_read();
  void reconnect();
  void moisture_1_read();
  void moisture_2_read();
  void moisture_3_read();
  void moisture_4_read(); 
  /*
  long lastMsg = 0;
  char msg[50];
  int value = 0;
  */
  //Adafruit_BME280 bme;
  
  float tempBME = 0.0;
  float humBME = 0.0;
  /*
  char *dtostrf (double val, signed char width, unsigned char prec, char *sout) {
    char fmt[20];
    sprintf(fmt, "%%%d.%df", width, prec);
    sprintf(sout, fmt, val);
    return sout;
  }
*/
#endif
