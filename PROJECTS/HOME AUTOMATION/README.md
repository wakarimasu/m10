<b>PROJECT: HOME AUTOMATION</b><br>

<div><p align="center">An artistic impression of a Home Automatin builded upon WIZcube hardware</p></div>
<div><p align="center"><img src="HA.jpg"></p></div>

All WIZcube modules are using W5100S-EVB-Pico module and the future release the WiFi version WizFi360-EVB-Pico

A home Automation project with intelligent nodes connected with NODE-RED and MQTT.


Easy to connect with <a href="https://www.home-assistant.io/"  target="_blank"><b> Home Assistant</b></a> or <a href="https://www.openhab.org"  target="_blank"><b> OpenHub</b></a> and be the best companion for every HA project no matter how small or big that is.
The nodes can collect and store info locally and take action when needed.

A firmware is under development, then boards can be easily configured and work in a "TASMOT" or "ESPHOME" like environment in a HA system.<br>
We may call it "WIZCUBEHome" but the name is not important. What is important, is the capabilities arizen with this type of hardware concept. The WIZcube ecosystem.<br> 
A hardware/software like that will have no limits in expansion and modularity.<br>
WIZcube modules that can be part of the Home Automation are:<br>

<a href="https://gitlab.com/m10cube/m10/-/tree/master/ELECTRICAL/INPUT%20OUTPUT/M10DX01-20" target="_blank"><b>M10DX01-20</b></a> 8X24V INPUT 8X24V OUTPUT HIGH SIDE: Switching large Relay or SSR for heavy loads e.g heating water tanks that require 20 amps load.<br>
<a href="https://gitlab.com/m10cube/m10/-/tree/master/ELECTRICAL/INPUT%20OUTPUT/M10DX02-20" target="_blank"><b>M10DX02-20</b></a> 8X24V INPUT 8XSSR zero crossing TRIAC: Control 220V AC loads up to 16A . Dimming as well can be possible using non zero crossing TRIAC.<br>
<a href="https://gitlab.com/m10cube/m10/-/tree/master/ELECTRICAL/OUTPUT/M10RL01-20" target="_blank"><b>M10RL01-20</b></a> 8XRELAY: Control AC or DC loads.<br>
<a href="https://gitlab.com/m10cube/m10/-/tree/master/ELECTRICAL/HMI" target="_blank"><b>M10HM01-20</b></a> A 24V / 5V Power Supply Unit<br>
<a href="https://gitlab.com/m10cube/m10/-/tree/master/ELECTRICAL/HMI" target="_blank"><b>M10HM01-20</b></a> A Human Machine Interface module (HMI) to be used for user interfasing with the HA. HMI modules can be placed anywhere in home, as many as you like. Communication via Modbus TCP/IP or MQTT.<br>
<p>Work in progrees...</p>

<H2>License</H2><p>
<img src="OSHWA-GR000004.jpg">

Verification code <a href="https://certification.oshwa.org/gr000004.html"> GR000004</a>

Licensed under the <a href="https://gitlab.com/m10cube/m10/-/blob/master/LICENCE.txt"> CERN OHL P 2.0 </a>
Software License: GPL v3<br>
Documentation License: CC BY 4.0 International<br>
