#include <SPI.h>
#include <EthernetClient.h>
#include <Ethernet.h>
#include <ArduinoRS485.h>
#include <ArduinoModbus.h>

#include "Free_Fonts.h"
#include <TFT_eSPI.h>
#include <string.h>

/** ETHERNET **/
// Enter a MAC address for your controller below.
// Newer Ethernet shields have a MAC address printed on a sticker on the shield
// The IP address will be dependent on your local network:
byte mac[] = {
  0xDE, 0xAD, 0xBE, 0xEF, 0xFE, 0xED
};
IPAddress ip(192, 168, 1, 212);
IPAddress server(192, 168, 1, 214); // update with the IP Address of your Modbus server

EthernetClient ethClient;

/** MODBUS **/
ModbusTCPClient modbusTCPClient(ethClient);

/*' TFT DISPLAY **/
// Use hardware SPI
TFT_eSPI tft = TFT_eSPI();

/** KEYPAD **/
uint8_t keypad[4][4];
const char keypad_char[4][4] = {{'1', '2', '3', 'A'}, {'4', '5', '6', 'B'}, {'7', '8', '9', 'C'}, {'*', '0', '#', 'D'}};

/** ENCODER **/
#define ENCODER_COUNT 3

typedef struct {
  uint8_t pinA;
  uint8_t pinB;
  int16_t count;
  bool flag;
  uint8_t scale;
} encoder_t;

encoder_t encoder[3] = {{8, 9, 0, false, 2}, {22, 26, 0, false, 2}, {27, 28, 0, false, 2}};

/** LED BUILTIN **/
const int ledPin = LED_BUILTIN;

/*******''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''''*/

void setup() {
 // Open serial communications
  Serial.begin(9600);
  Serial.println("WIZCube Remote Control Example");

  /** Ethernet init **/
  // Init ethernet with correct CS pin for WIZnet EVB Pico
  Ethernet.init(17);
  // start the ethernet connection and the server:
  Ethernet.begin(mac, ip);

  // Check for Ethernet hardware present
  if (Ethernet.hardwareStatus() == EthernetNoHardware) {
    Serial.println("Ethernet shield was not found.  Sorry, can't run without hardware. :(");
  }
  // Check if the ethernet wire is connected
  if (Ethernet.linkStatus() == LinkOFF) {
    Serial.println("Ethernet cable is not connected.");
  }

  // Print own IP address
  Serial.println(Ethernet.localIP());

  /** TFT Display init **/
  tft.begin();
  tft.setTextDatum(TL_DATUM);
  // Set text colour to orange with black background
  tft.setTextColor(TFT_WHITE, TFT_BLACK);
  tft.fillScreen(TFT_BLACK);              // Clear screen
  tft.setFreeFont(FF1);                   // Select the font
  tft.drawString("Inputs:", 0, 0);
  tft.drawString("Outputs:", 0, 30); 
  tft.drawString("Encoder: 0", 0, 60);  
  tft.drawString("Received: 0", 0, 90); 
  tft.setRotation(1);    

  /** Keypad init **/
  for(int i=0; i<8; i++) {
    _gpio_init(i);
    gpio_set_dir(i, false);
    gpio_pull_up(i);
  }

  /** Encoder init **/
  for(int i=0; i < ENCODER_COUNT; i++) {
    _gpio_init(encoder[i].pinA);
    _gpio_init(encoder[i].pinB);
    gpio_pull_up(encoder[i].pinA);
    gpio_pull_up(encoder[i].pinB);
    gpio_set_irq_enabled_with_callback(encoder[i].pinA, GPIO_IRQ_EDGE_RISE, true, gpio_irq_callback);
  }
  
  // configure the LED
  pinMode(ledPin, OUTPUT);
  digitalWrite(ledPin, LOW);
  
  Serial.println("Setup done");
}

// Process the Encoder signal 
inline void encoder_process(uint8_t encNum, uint32_t events) {
  bool pinAState = false;
  bool pinBState = gpio_get(encoder[encNum].pinB) != 0 ? true : false;
  
  if(events == GPIO_IRQ_EDGE_RISE)
        pinAState = true;
  if(pinAState == pinBState)
    ++encoder[encNum].count;
  else
    --encoder[encNum].count;
  encoder[encNum].flag = true;
}

// GPIO IRQ Callback for the Encoder signal
void gpio_irq_callback(uint gpio, uint32_t events) {
  
    if(gpio == encoder[0].pinA)
      encoder_process(0, events);
    else if(gpio == encoder[1].pinA)
      encoder_process(1, events);
    else if(gpio == encoder[2].pinA)
      encoder_process(2, events);      
}

void loop() {

  static uint8_t inputByte = 0x00;
  static uint8_t prevInByte = 0x00;
  static uint8_t outputByte = 0x00;
  static uint8_t prevOutByte = 0x00;
  static int prevEncoderReceivedValue = 0;
  static char PrevKey = NULL;
  static uint8_t keyDebounce = 0;
  int encoderCountReceived;
  char inputString[20] = "Inputs: ";
  char outputString[20] = "Outputs: ";
  char encoderString[30] = "";
  char keyPressed = NULL;

  if (!modbusTCPClient.connected()) {
    // client not connected, start the Modbus TCP client
    Serial.println("Attempting to connect to Modbus TCP server");
    
    if (!modbusTCPClient.begin(server, 502)) {
      Serial.println("Modbus TCP Client failed to connect!");
    } else {
      Serial.println("Modbus TCP Client connected");
    }
  } else {
    
    modbusTCPClient.holdingRegisterWrite(0x02, encoder[0].count);
    
    if( outputByte != prevOutByte) { 
      if(!modbusTCPClient.beginTransmission(COILS, 0x00, 8)) {
        Serial.print("Failed to begin transmission ! ");
        Serial.println(modbusTCPClient.lastError());
      }
      else {
        for(int i=0; i < 8; i++) {
          modbusTCPClient.write((outputByte >> i) & 0x01);
        } 
      }
      if(!modbusTCPClient.endTransmission()) {
        Serial.print("Failed to write coil! ");
        Serial.println(modbusTCPClient.lastError());
      }
      else
        Serial.println("Multiple Coil command sent! ");
      prevOutByte = outputByte; 
    }
    
    inputByte = ~(uint8_t)modbusTCPClient.holdingRegisterRead(0x00);
    encoderCountReceived = (int)modbusTCPClient.holdingRegisterRead(0x02);
  }

  if(encoderCountReceived != prevEncoderReceivedValue) {
    sprintf(encoderString, "Received: %hi      ", encoderCountReceived);
    tft.drawString(encoderString, 0, 90);
    prevEncoderReceivedValue = encoderCountReceived;
  }

  if(inputByte != prevInByte) {
     tft_write_io_byte(inputString, 7, inputByte);
     tft.drawString(inputString, 0, 0);  
     prevInByte = inputByte;
  }
  
  if(encoder[0].flag) {
    encoder[0].flag = false;
    sprintf(encoderString, "Encoder: %i     ", encoder[0].count);
    tft.drawString(encoderString, 0, 60);
    outputByte = 0x01 << (abs(encoder[0].count) % 8);
    tft_write_io_byte(outputString, 7, outputByte);
     tft.drawString(outputString, 0, 30);
  }
}

// Scans the keypad 
bool keypad_scan(char* c) {
  for(int row=0; row<4; row++) {
      gpio_set_dir(row, true);
      gpio_put(row, false);
      delayMicroseconds(1);
      for(int col=0; col<4; col++) {
        if(!gpio_get(col+4)) {
          *c = keypad_char[row][col];
          gpio_set_dir(row, false);
          return true;
        }
      }
      gpio_set_dir(row, false);
  }
  return false;
}

void tft_write_io_byte(char* text, uint8_t pos, uint8_t data) {
  for(int i=0; i<8; i++) {
    if(data & (1 << i))
      sprintf(&text[pos+i], "%c", (i + 0x31));
    else
      sprintf(&text[pos+i], "  ");
  }
  sprintf(&text[pos+8], "  ");
}
