M10DX02-10<br> 
8 x Digital Opto Isolated Inputs (24V DC) plus 8 x Digital Outputs (High Side Transistor 24V DC)
<p align="center"><img src="/ELECTRICAL/IMAGES/M10DX02-10-3DTOP.png"></p>

M10DX01-20 WIZcube version<br>
WIZnet PICO clone on board<br>
8 x Digital Opto Isolated Inputs (24V DC) plus 8 x Digital Outputs (High Side Transistor 24V DC) 
<p align="center"><img src="/ELECTRICAL/IMAGES/M10DX01-20-3DTOP.png"></p>

M10DX02-20 WIZcube version<br>
WIZnet PICO clone on board<br>
8 x Digital Opto Isolated Inputs (24V DC) plus 8 x Digital Outputs SSR (TRIAC 220V DC)
<p align="center"><img src="/ELECTRICAL/IMAGES/M10DX02-20-3DTOP.png"></p>

<H2>License</H2><p>
<img src="OSHWA-GR000004.jpg">

Verification code <a href="https://certification.oshwa.org/gr000004.html"> GR000004</a>

Licensed under the <a href="https://gitlab.com/m10cube/m10/-/blob/master/LICENCE.txt"> CERN OHL P 2.0 </a>
Software License: GPL v3<br>
Documentation License: CC BY 4.0 International<br> 





