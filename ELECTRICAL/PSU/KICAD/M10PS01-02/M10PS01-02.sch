EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr User 16774 9538
encoding utf-8
Sheet 1 1
Title "M10PS01"
Date "2020-10-22"
Rev "01"
Comp "M10CUBE"
Comment1 ""
Comment2 "Output TS1117 +3V3"
Comment3 "Output 2 x MP1584 modules +5V"
Comment4 "Power Supply  module 24V Input"
$EndDescr
Wire Wire Line
	2075 6350 1575 6350
Text Label 1575 6350 0    70   ~ 0
3V3_1
Wire Wire Line
	7275 7150 7125 7150
Wire Wire Line
	7125 7150 7125 6550
Wire Wire Line
	7125 6550 7125 6150
Wire Wire Line
	7125 6150 6975 6150
Wire Wire Line
	6975 6150 6675 6150
Wire Wire Line
	6675 6150 6275 6150
Wire Wire Line
	6675 6250 6675 6150
Wire Wire Line
	6975 6250 6975 6150
Wire Wire Line
	7275 6550 7125 6550
Connection ~ 6675 6150
Connection ~ 6975 6150
Connection ~ 7125 6550
Text Label 6275 6150 0    70   ~ 0
3V3_1
Wire Wire Line
	2075 6450 1575 6450
Text Label 1575 6450 0    70   ~ 0
SDA
Wire Wire Line
	7175 6750 6675 6750
Wire Wire Line
	6675 6750 6275 6750
Wire Wire Line
	6675 6650 6675 6750
Connection ~ 6675 6750
Text Label 6275 6750 0    70   ~ 0
SDA
Wire Wire Line
	2075 6550 1575 6550
Text Label 1575 6550 0    70   ~ 0
SCL
Wire Wire Line
	7175 7350 6975 7350
Wire Wire Line
	6975 7350 6275 7350
Wire Wire Line
	6975 6650 6975 7350
Connection ~ 6975 7350
Text Label 6275 7350 0    70   ~ 0
SCL
Wire Wire Line
	2075 6650 1575 6650
Text Label 1575 6650 0    70   ~ 0
GPIO_4
Wire Wire Line
	4375 6650 3875 6650
Text Label 3875 6650 0    70   ~ 0
GPIO_4
Wire Wire Line
	2075 6750 1575 6750
Wire Wire Line
	4375 6750 3875 6750
Text Label 3875 6750 0    70   ~ 0
GND_1
Wire Wire Line
	2075 6850 1575 6850
Text Label 1575 6850 0    70   ~ 0
GPIO_17
Wire Wire Line
	4375 6850 3875 6850
Text Label 3875 6850 0    70   ~ 0
GPIO_17
Wire Wire Line
	2075 6950 1575 6950
Text Label 1575 6950 0    70   ~ 0
GPIO_27
Wire Wire Line
	4375 6950 3875 6950
Text Label 3875 6950 0    70   ~ 0
GPIO_27
Wire Wire Line
	2075 7050 1575 7050
Text Label 1575 7050 0    70   ~ 0
GPIO_22
Wire Wire Line
	4375 7050 3875 7050
Text Label 3875 7050 0    70   ~ 0
GPIO_22
Wire Wire Line
	2075 7150 1575 7150
Text Label 1575 7150 0    70   ~ 0
3V3_2
Wire Wire Line
	4375 7150 3875 7150
Text Label 3875 7150 0    70   ~ 0
3V3_2
Wire Wire Line
	2075 7250 1575 7250
Text Label 1575 7250 0    70   ~ 0
MOSI
Wire Wire Line
	4375 7250 3875 7250
Text Label 3875 7250 0    70   ~ 0
MOSI
Wire Wire Line
	2075 7350 1575 7350
Text Label 1575 7350 0    70   ~ 0
MISO
Wire Wire Line
	4375 7350 3875 7350
Text Label 3875 7350 0    70   ~ 0
MISO
Wire Wire Line
	2075 7450 1575 7450
Text Label 1575 7450 0    70   ~ 0
SCLK
Wire Wire Line
	4375 7450 3875 7450
Text Label 3875 7450 0    70   ~ 0
SCLK
Wire Wire Line
	2075 7550 1575 7550
Wire Wire Line
	4375 7550 3875 7550
Text Label 3875 7550 0    70   ~ 0
GND_2
Wire Wire Line
	2075 7650 1575 7650
Text Label 1575 7650 0    70   ~ 0
GPIO_0
Wire Wire Line
	4375 7650 3875 7650
Text Label 3875 7650 0    70   ~ 0
GPIO_0
Wire Wire Line
	2075 7750 1575 7750
Text Label 1575 7750 0    70   ~ 0
GPIO_5
Wire Wire Line
	4375 7750 3875 7750
Text Label 3875 7750 0    70   ~ 0
GPIO_5
Wire Wire Line
	2075 7850 1575 7850
Text Label 1575 7850 0    70   ~ 0
GPIO_6
Wire Wire Line
	4375 7850 3875 7850
Text Label 3875 7850 0    70   ~ 0
GPIO_6
Wire Wire Line
	2075 7950 1575 7950
Text Label 1575 7950 0    70   ~ 0
GPIO_13
Wire Wire Line
	4375 7950 3875 7950
Text Label 3875 7950 0    70   ~ 0
GPIO_13
Wire Wire Line
	2075 8050 1575 8050
Text Label 1575 8050 0    70   ~ 0
GPIO_19
Wire Wire Line
	4375 8050 3875 8050
Text Label 3875 8050 0    70   ~ 0
GPIO_19
Wire Wire Line
	2075 8150 1575 8150
Text Label 1575 8150 0    70   ~ 0
GPIO_26
Wire Wire Line
	4375 8150 3875 8150
Text Label 3875 8150 0    70   ~ 0
GPIO_26
Wire Wire Line
	2075 8250 1575 8250
Wire Wire Line
	4375 8250 3875 8250
Text Label 3875 8250 0    70   ~ 0
GND_3
Wire Wire Line
	2675 6550 3175 6550
Text Label 3175 6550 0    70   ~ 0
GND_4
Wire Wire Line
	4975 6550 5475 6550
Text Label 5075 6550 0    70   ~ 0
GND_4
Wire Wire Line
	2675 6650 3175 6650
Text Label 3175 6650 0    70   ~ 0
TXD
Wire Wire Line
	4975 6650 5475 6650
Text Label 5475 6650 0    70   ~ 0
TXD
Wire Wire Line
	2675 6850 3175 6850
Text Label 3175 6850 0    70   ~ 0
GPIO_18
Wire Wire Line
	4975 6850 5475 6850
Text Label 5475 6850 0    70   ~ 0
GPIO_18
Wire Wire Line
	2675 6950 3175 6950
Text Label 3175 6950 0    70   ~ 0
GND_5
Wire Wire Line
	4975 6950 5475 6950
Text Label 5075 6950 0    70   ~ 0
GND_5
Wire Wire Line
	2675 7050 3175 7050
Text Label 3175 7050 0    70   ~ 0
GPIO_23
Wire Wire Line
	4975 7050 5475 7050
Text Label 5475 7050 0    70   ~ 0
GPIO_23
Wire Wire Line
	2675 7150 3175 7150
Text Label 3175 7150 0    70   ~ 0
GPIO_24
Wire Wire Line
	4975 7150 5475 7150
Text Label 5475 7150 0    70   ~ 0
GPIO_24
Wire Wire Line
	2675 7250 3175 7250
Text Label 3175 7250 0    70   ~ 0
GND_6
Wire Wire Line
	4975 7250 5475 7250
Text Label 5075 7250 0    70   ~ 0
GND_6
Wire Wire Line
	2675 7350 3175 7350
Text Label 3175 7350 0    70   ~ 0
GPIO_25
Wire Wire Line
	4975 7350 5475 7350
Text Label 5475 7350 0    70   ~ 0
GPIO_25
Wire Wire Line
	2675 7650 3175 7650
Text Label 3175 7650 0    70   ~ 0
GPIO_1
Wire Wire Line
	4975 7650 5475 7650
Text Label 5475 7650 0    70   ~ 0
GPIO_1
Wire Wire Line
	2675 7750 3175 7750
Text Label 3175 7750 0    70   ~ 0
GND_7
Wire Wire Line
	4975 7750 5475 7750
Text Label 5075 7750 0    70   ~ 0
GND_7
Wire Wire Line
	2675 7850 3175 7850
Text Label 3175 7850 0    70   ~ 0
GPIO_12
Wire Wire Line
	4975 7850 5475 7850
Text Label 5475 7850 0    70   ~ 0
GPIO_12
Wire Wire Line
	2675 7950 3175 7950
Text Label 3175 7950 0    70   ~ 0
GND_8
Wire Wire Line
	4975 7950 5475 7950
Text Label 5075 7950 0    70   ~ 0
GND_8
Wire Wire Line
	2675 8050 3175 8050
Text Label 3175 8050 0    70   ~ 0
GPIO_16
Wire Wire Line
	4975 8050 5475 8050
Text Label 5475 8050 0    70   ~ 0
GPIO_16
Wire Wire Line
	2675 8150 3175 8150
Text Label 3175 8150 0    70   ~ 0
GPIO_20
Wire Wire Line
	4975 8150 5475 8150
Text Label 5475 8150 0    70   ~ 0
GPIO_20
Wire Wire Line
	2675 8250 3175 8250
Text Label 3175 8250 0    70   ~ 0
GPIO_21
Wire Wire Line
	4975 8250 5475 8250
Text Label 5475 8250 0    70   ~ 0
GPIO_21
Wire Wire Line
	4975 7450 5475 7450
Text Label 5475 7450 0    70   ~ 0
CE0
Wire Wire Line
	2675 7450 3175 7450
Text Label 3175 7450 0    70   ~ 0
CE0
Wire Wire Line
	4975 7550 5475 7550
Text Label 5475 7550 0    70   ~ 0
CE1
Wire Wire Line
	2675 7550 3175 7550
Text Label 3175 7550 0    70   ~ 0
CE1
Wire Wire Line
	11175 6350 11175 6250
Wire Wire Line
	12075 6350 12075 6250
Wire Wire Line
	10875 6350 10875 6250
Text Label 10875 6350 0    70   ~ 0
GND
Wire Wire Line
	4775 2600 4275 2600
Wire Wire Line
	4275 2050 4275 2600
Wire Wire Line
	4775 2050 4775 2600
Connection ~ 4275 2600
Wire Wire Line
	11875 5850 12075 5850
Wire Wire Line
	12075 5850 12075 5950
Wire Wire Line
	11875 5950 12075 5950
Wire Wire Line
	12075 5850 12525 5850
Connection ~ 12075 5950
Connection ~ 12075 5850
Text Label 12125 5850 0    70   ~ 0
3V3_BUS
Wire Wire Line
	4375 6350 3875 6350
Text Label 3875 6350 0    70   ~ 0
3V3_BUS
Wire Wire Line
	11275 5850 11175 5850
Wire Wire Line
	11175 5850 10875 5850
Wire Wire Line
	10875 5850 10325 5850
Wire Wire Line
	10875 5950 10875 5850
Wire Wire Line
	11175 5950 11175 5850
Connection ~ 10875 5850
Connection ~ 11175 5850
Text Label 10425 5850 0    70   ~ 0
5V_BUS
Text Label 7125 1300 0    70   ~ 0
5V_BUS
Wire Wire Line
	4975 6450 5475 6450
Wire Wire Line
	5475 6450 5475 6350
Wire Wire Line
	5475 6350 4975 6350
Text Label 5075 6350 0    70   ~ 0
5V_BUS
Wire Wire Line
	7675 6250 7675 6150
Wire Wire Line
	7675 6150 7975 6150
Wire Wire Line
	7975 6150 8475 6150
Wire Wire Line
	7975 6250 7975 6150
Connection ~ 7975 6150
Text Label 8175 6150 0    70   ~ 0
5V_BUS
Wire Wire Line
	3625 1300 2975 1300
Text Label 3275 1300 0    70   ~ 0
24V
Wire Wire Line
	4025 1300 4275 1300
Wire Wire Line
	4275 1300 4775 1300
Wire Wire Line
	4775 1300 5400 1300
Wire Wire Line
	4275 1750 4275 1300
Wire Wire Line
	4775 1750 4775 1300
Connection ~ 4275 1300
Connection ~ 4775 1300
Text Label 4375 1300 0    70   ~ 0
24V_FUSE
Text Label 7325 2800 0    70   ~ 0
5V_RPI
Wire Wire Line
	2675 6450 3175 6450
Wire Wire Line
	3175 6450 3175 6350
Wire Wire Line
	3175 6350 2675 6350
Text Label 2775 6350 0    70   ~ 0
5V_RPI
Wire Wire Line
	2675 6750 3175 6750
Text Label 3175 6750 0    70   ~ 0
RXD
Wire Wire Line
	4975 6750 5475 6750
Text Label 5475 6750 0    70   ~ 0
RXD
Wire Wire Line
	4375 6450 3875 6450
Text Label 3875 6450 0    70   ~ 0
SDA_BUS
Wire Wire Line
	7575 6750 7675 6750
Wire Wire Line
	7675 6750 8475 6750
Wire Wire Line
	7675 6650 7675 6750
Connection ~ 7675 6750
Text Label 8175 6750 0    70   ~ 0
SDA_BUS
Wire Wire Line
	4375 6550 3875 6550
Text Label 3875 6550 0    70   ~ 0
SCL_BUS
Wire Wire Line
	7575 7350 7975 7350
Wire Wire Line
	7975 7350 8475 7350
Wire Wire Line
	7975 6650 7975 7350
Connection ~ 7975 7350
Text Label 8175 7350 0    70   ~ 0
SCL_BUS
$Comp
L M10PS01-01-rescue:M20X2-RPi_a-eagle-import-m10-psu-rescue-m10-psu-01-rescue CON_1
U 1 1 0001D5F7
P 2375 7350
F 0 "CON_1" H 2275 8500 59  0000 L BNN
F 1 "M20X2" H 2275 6250 59  0000 L BNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x20_P2.54mm_Vertical" H 2375 7350 50  0001 C CNN
F 3 "" H 2375 7350 50  0001 C CNN
	1    2375 7350
	1    0    0    -1  
$EndComp
$Comp
L M10PS01-01-rescue:M20X2-RPi_a-eagle-import-m10-psu-rescue-m10-psu-01-rescue CON_2
U 1 1 7B641FE1
P 4675 7350
F 0 "CON_2" H 4575 8500 59  0000 L BNN
F 1 "M20X2" H 4575 6250 59  0000 L BNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_2x20_P2.54mm_Vertical" H 4675 7350 50  0001 C CNN
F 3 "" H 4675 7350 50  0001 C CNN
	1    4675 7350
	1    0    0    -1  
$EndComp
$Comp
L M10PS01-01-rescue:DC_DC-RPi_a-eagle-import-m10-psu-rescue-m10-psu-01-rescue U$2
U 1 1 A8B04D1A
P 5975 1600
F 0 "U$2" H 5675 2100 59  0000 L TNN
F 1 "DC_DC-MP1584" H 5675 1100 59  0000 L BNN
F 2 "SWITCHING:DC_DC" H 5975 1600 50  0001 C CNN
F 3 "" H 5975 1600 50  0001 C CNN
	1    5975 1600
	1    0    0    1   
$EndComp
$Comp
L M10PS01-01-rescue:C-EUC0603-RPi_a-eagle-import-m10-psu-rescue-m10-psu-01-rescue C2
U 1 1 69098DAB
P 11175 6050
F 0 "C2" H 11235 6065 59  0000 L BNN
F 1 "100n" H 11235 5865 59  0000 L BNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 11175 6050 50  0001 C CNN
F 3 "" H 11175 6050 50  0001 C CNN
	1    11175 6050
	1    0    0    -1  
$EndComp
$Comp
L M10PS01-01-rescue:V_REG_LM1117SOT223-RPi_a-eagle-import-m10-psu-rescue-m10-psu-01-rescue IC2
U 1 1 9E4D1D00
P 11575 5850
F 0 "IC2" H 11675 5550 59  0000 L BNN
F 1 "TS1117" H 11675 5450 59  0000 L BNN
F 2 "Package_TO_SOT_SMD:SOT-223" H 11575 5850 50  0001 C CNN
F 3 "" H 11575 5850 50  0001 C CNN
	1    11575 5850
	1    0    0    -1  
$EndComp
$Comp
L M10PS01-01-rescue:C-EUC1206-RPi_a-eagle-import-m10-psu-rescue-m10-psu-01-rescue C16
U 1 1 C8289A19
P 12075 6050
F 0 "C16" H 12135 6065 59  0000 L BNN
F 1 "10µF" H 12135 5865 59  0000 L BNN
F 2 "Capacitor_SMD:C_Elec_3x5.4" H 12075 6050 50  0001 C CNN
F 3 "" H 12075 6050 50  0001 C CNN
	1    12075 6050
	1    0    0    -1  
$EndComp
$Comp
L M10PS01-01-rescue:C-EUC1206-RPi_a-eagle-import-m10-psu-rescue-m10-psu-01-rescue C14
U 1 1 540115D4
P 10875 6050
F 0 "C14" H 10935 6065 59  0000 L BNN
F 1 "10µF" H 10935 5865 59  0000 L BNN
F 2 "Capacitor_SMD:C_Elec_3x5.4" H 10875 6050 50  0001 C CNN
F 3 "" H 10875 6050 50  0001 C CNN
	1    10875 6050
	1    0    0    -1  
$EndComp
$Comp
L M10PS01-01-rescue:TE5-RPi_a-eagle-import-m10-psu-rescue-m10-psu-01-rescue F2
U 1 1 2CCB2C12
P 3825 1300
F 0 "F2" H 3675 1355 59  0000 L BNN
F 1 "2A" H 3675 1185 59  0000 L BNN
F 2 "FUSE:TE5" H 3825 1300 50  0001 C CNN
F 3 "" H 3825 1300 50  0001 C CNN
	1    3825 1300
	1    0    0    -1  
$EndComp
$Comp
L Device:CP C17
U 1 1 8196D577
P 4275 1900
F 0 "C17" H 3970 1869 59  0000 L BNN
F 1 "47µ / 35V" H 3720 1769 59  0000 L BNN
F 2 "Capacitor_SMD:CP_Elec_8x10" H 4275 1900 50  0001 C CNN
F 3 "" H 4275 1900 50  0001 C CNN
	1    4275 1900
	1    0    0    -1  
$EndComp
$Comp
L M10PS01-01-rescue:C-EUC0805-RPi_a-eagle-import-m10-psu-rescue-m10-psu-01-rescue C18
U 1 1 52B654BE
P 4775 1950
F 0 "C18" H 4935 1815 59  0000 L BNN
F 1 "100n" H 4935 1915 59  0000 L BNN
F 2 "Capacitor_SMD:C_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 4775 1950 50  0001 C CNN
F 3 "" H 4775 1950 50  0001 C CNN
	1    4775 1950
	-1   0    0    1   
$EndComp
$Comp
L M10PS01-01-rescue:DIODE-DO214AA-RPi_a-eagle-import-m10-psu-rescue-m10-psu-01-rescue D1
U 1 1 A45C43A8
P 2875 1300
F 0 "D1" H 2975 1319 59  0000 L BNN
F 1 "B340A" H 2975 1209 59  0000 L BNN
F 2 "DIODE:DO214AA" H 2875 1300 50  0001 C CNN
F 3 "" H 2875 1300 50  0001 C CNN
	1    2875 1300
	1    0    0    -1  
$EndComp
$Comp
L M10PS01-01-rescue:MOSFET-N-SOT23-RPi_a-eagle-import-m10-psu-rescue-m10-psu-01-rescue T1
U 1 1 A63F0DFA
P 7375 6750
F 0 "T1" V 7575 6650 59  0000 L BNN
F 1 "BSS138" V 7675 6650 59  0000 L BNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 7375 6750 50  0001 C CNN
F 3 "" H 7375 6750 50  0001 C CNN
	1    7375 6750
	0    1    1    0   
$EndComp
$Comp
L M10PS01-01-rescue:MOSFET-N-SOT23-RPi_a-eagle-import-m10-psu-rescue-m10-psu-01-rescue T2
U 1 1 39676F95
P 7375 7350
F 0 "T2" V 7575 7250 59  0000 L BNN
F 1 "BSS138" V 7675 7250 59  0000 L BNN
F 2 "Package_TO_SOT_SMD:SOT-23" H 7375 7350 50  0001 C CNN
F 3 "" H 7375 7350 50  0001 C CNN
	1    7375 7350
	0    1    1    0   
$EndComp
$Comp
L M10PS01-01-rescue:R-EU_M0805-RPi_a-eagle-import-m10-psu-rescue-m10-psu-01-rescue R1
U 1 1 B9BAFB95
P 6675 6450
F 0 "R1" H 6525 6509 59  0000 L BNN
F 1 "10k" H 6725 6520 59  0000 L BNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6675 6450 50  0001 C CNN
F 3 "" H 6675 6450 50  0001 C CNN
	1    6675 6450
	0    -1   -1   0   
$EndComp
$Comp
L M10PS01-01-rescue:R-EU_M0805-RPi_a-eagle-import-m10-psu-rescue-m10-psu-01-rescue R2
U 1 1 81BCC900
P 6975 6450
F 0 "R2" H 6825 6509 59  0000 L BNN
F 1 "10k" H 7025 6520 59  0000 L BNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 6975 6450 50  0001 C CNN
F 3 "" H 6975 6450 50  0001 C CNN
	1    6975 6450
	0    -1   -1   0   
$EndComp
$Comp
L M10PS01-01-rescue:R-EU_M0805-RPi_a-eagle-import-m10-psu-rescue-m10-psu-01-rescue R3
U 1 1 26020ECB
P 7675 6450
F 0 "R3" H 7525 6509 59  0000 L BNN
F 1 "10k" H 7725 6520 59  0000 L BNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7675 6450 50  0001 C CNN
F 3 "" H 7675 6450 50  0001 C CNN
	1    7675 6450
	0    -1   -1   0   
$EndComp
$Comp
L M10PS01-01-rescue:R-EU_M0805-RPi_a-eagle-import-m10-psu-rescue-m10-psu-01-rescue R4
U 1 1 98DC0D12
P 7975 6450
F 0 "R4" H 7825 6509 59  0000 L BNN
F 1 "10k" H 8025 6520 59  0000 L BNN
F 2 "Resistor_SMD:R_0805_2012Metric_Pad1.15x1.40mm_HandSolder" H 7975 6450 50  0001 C CNN
F 3 "" H 7975 6450 50  0001 C CNN
	1    7975 6450
	0    -1   -1   0   
$EndComp
Text Notes 10875 5550 0    85   ~ 0
5V to 3.3V BUS
Text Notes 4525 8700 0    59   ~ 0
Bus Stecker
Text Notes 2225 8700 0    59   ~ 0
RPi Stecker
Text Notes 2025 6000 0    50   ~ 0
RASPBERRY CONNECTOR
$Comp
L Connector_Generic:Conn_01x04 CON_3
U 1 1 5F448C0E
P 2075 1400
F 0 "CON_3" H 1993 1717 50  0000 C CNN
F 1 "Conn_01x04" H 1993 1626 50  0000 C CNN
F 2 "Connector_Phoenix_MC:PhoenixContact_MC_1,5_4-G-3.81_1x04_P3.81mm_Horizontal" H 1993 1067 50  0001 C CNN
F 3 "~" H 2075 1400 50  0001 C CNN
	1    2075 1400
	-1   0    0    -1  
$EndComp
Wire Wire Line
	2275 1300 2775 1300
Wire Wire Line
	2275 1400 2275 1300
Wire Wire Line
	2275 1500 2275 1600
Wire Wire Line
	2725 2600 4275 2600
Wire Wire Line
	2275 1600 2725 1600
Wire Wire Line
	2725 1600 2725 2600
Connection ~ 2275 1600
Connection ~ 2275 1300
$Comp
L Mechanical:MountingHole H1
U 1 1 5F6676D5
P 13875 1300
F 0 "H1" H 13975 1346 50  0000 L CNN
F 1 "MountingHole" H 13975 1255 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.5mm_Pad_Via" H 13875 1300 50  0001 C CNN
F 3 "~" H 13875 1300 50  0001 C CNN
	1    13875 1300
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H2
U 1 1 5F668877
P 13875 1550
F 0 "H2" H 13975 1596 50  0000 L CNN
F 1 "MountingHole" H 13975 1505 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.5mm_Pad_Via" H 13875 1550 50  0001 C CNN
F 3 "~" H 13875 1550 50  0001 C CNN
	1    13875 1550
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H3
U 1 1 5F668E2C
P 13875 1800
F 0 "H3" H 13975 1846 50  0000 L CNN
F 1 "MountingHole" H 13975 1755 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.5mm_Pad_Via" H 13875 1800 50  0001 C CNN
F 3 "~" H 13875 1800 50  0001 C CNN
	1    13875 1800
	1    0    0    -1  
$EndComp
$Comp
L Mechanical:MountingHole H4
U 1 1 5F669B29
P 13875 2075
F 0 "H4" H 13975 2121 50  0000 L CNN
F 1 "MountingHole" H 13975 2030 50  0000 L CNN
F 2 "MountingHole:MountingHole_3.5mm_Pad_Via" H 13875 2075 50  0001 C CNN
F 3 "~" H 13875 2075 50  0001 C CNN
	1    13875 2075
	1    0    0    -1  
$EndComp
Text Label 4125 2600 2    50   ~ 0
GND_24
$Comp
L power:GND #PWR0101
U 1 1 5F71454F
P 6575 1900
F 0 "#PWR0101" H 6575 1650 50  0001 C CNN
F 1 "GND" V 6580 1772 50  0000 R CNN
F 2 "" H 6575 1900 50  0001 C CNN
F 3 "" H 6575 1900 50  0001 C CNN
	1    6575 1900
	1    0    0    -1  
$EndComp
Wire Wire Line
	4775 2600 5200 2600
Wire Wire Line
	5200 2600 5200 1800
Wire Wire Line
	5200 1800 5575 1800
Connection ~ 4775 2600
Wire Wire Line
	5575 1400 5575 1300
Wire Wire Line
	5575 1900 5575 1800
Wire Wire Line
	6575 1900 6575 1800
Wire Wire Line
	6575 2800 7325 2800
Wire Wire Line
	6575 1400 6575 1300
Wire Wire Line
	6575 3400 6575 3300
Wire Wire Line
	5575 2900 5575 2800
Wire Wire Line
	5575 3400 5575 3300
Wire Wire Line
	5400 1300 5400 2800
Wire Wire Line
	5400 2800 5575 2800
Connection ~ 5400 1300
Wire Wire Line
	5400 1300 5575 1300
Wire Wire Line
	5200 2600 5200 3300
Connection ~ 5200 2600
$Comp
L power:GND #PWR0102
U 1 1 5F8B779E
P 6575 3400
F 0 "#PWR0102" H 6575 3150 50  0001 C CNN
F 1 "GND" V 6580 3272 50  0000 R CNN
F 2 "" H 6575 3400 50  0001 C CNN
F 3 "" H 6575 3400 50  0001 C CNN
	1    6575 3400
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 5F8CE432
P 4775 2600
F 0 "#PWR0103" H 4775 2350 50  0001 C CNN
F 1 "GND" V 4780 2472 50  0000 R CNN
F 2 "" H 4775 2600 50  0001 C CNN
F 3 "" H 4775 2600 50  0001 C CNN
	1    4775 2600
	1    0    0    -1  
$EndComp
Wire Wire Line
	5200 3300 5575 3300
Connection ~ 6575 3400
Connection ~ 5575 3300
Connection ~ 5575 2800
$Comp
L M10PS01-01-rescue:DC_DC-RPi_a-eagle-import-m10-psu-rescue-m10-psu-01-rescue U$3
U 1 1 54307519
P 5975 3100
F 0 "U$3" H 5675 3600 59  0000 L TNN
F 1 "DC_DC-MP1584" H 5675 2600 59  0000 L BNN
F 2 "SWITCHING:DC_DC" H 5975 3100 50  0001 C CNN
F 3 "" H 5975 3100 50  0001 C CNN
	1    5975 3100
	1    0    0    1   
$EndComp
Connection ~ 6575 1900
Connection ~ 5575 1300
Connection ~ 5575 1800
Connection ~ 6575 1300
Wire Wire Line
	6575 1300 7125 1300
Wire Wire Line
	11575 6350 11575 6150
Text Label 1575 8250 0    70   ~ 0
GND_3
Text Label 1575 7550 0    70   ~ 0
GND_2
Text Label 1575 6750 0    70   ~ 0
GND_1
Text Label 5475 6950 0    70   ~ 0
GND
Text Label 5475 6550 0    70   ~ 0
GND
Text Label 5475 7250 0    70   ~ 0
GND
Text Label 5475 7750 0    70   ~ 0
GND
Text Label 5475 7950 0    70   ~ 0
GND
Text Label 6575 3400 0    70   ~ 0
GND
Connection ~ 6575 2800
Wire Wire Line
	6575 2900 6575 2800
$Comp
L power:GND #PWR0104
U 1 1 5F9EF5CD
P 10875 6350
F 0 "#PWR0104" H 10875 6100 50  0001 C CNN
F 1 "GND" V 10880 6222 50  0000 R CNN
F 2 "" H 10875 6350 50  0001 C CNN
F 3 "" H 10875 6350 50  0001 C CNN
	1    10875 6350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0105
U 1 1 5F9F405B
P 11175 6350
F 0 "#PWR0105" H 11175 6100 50  0001 C CNN
F 1 "GND" V 11180 6222 50  0000 R CNN
F 2 "" H 11175 6350 50  0001 C CNN
F 3 "" H 11175 6350 50  0001 C CNN
	1    11175 6350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0106
U 1 1 5F9F42E3
P 11575 6350
F 0 "#PWR0106" H 11575 6100 50  0001 C CNN
F 1 "GND" V 11580 6222 50  0000 R CNN
F 2 "" H 11575 6350 50  0001 C CNN
F 3 "" H 11575 6350 50  0001 C CNN
	1    11575 6350
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0107
U 1 1 5F9F4526
P 12075 6350
F 0 "#PWR0107" H 12075 6100 50  0001 C CNN
F 1 "GND" V 12080 6222 50  0000 R CNN
F 2 "" H 12075 6350 50  0001 C CNN
F 3 "" H 12075 6350 50  0001 C CNN
	1    12075 6350
	1    0    0    -1  
$EndComp
$EndSCHEMATC
